'use strict'
import { combineReducers } from 'redux';

import uiReducer from './uiReducer';

let reducer = combineReducers({
  ui: uiReducer
});

export default reducer;
